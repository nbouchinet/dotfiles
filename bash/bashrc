# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:$HOME/.bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$HOME/.bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

xset b off
shopt -s globstar
export EDITOR="nvim"
export VISUAL="nvim"

export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL=ignoreboth:erasedups

export TERM=tmux-256color
export PS1="\[\e[m\]$PS1\$(parse_git_branch)\n> "
export CLIPOS_HOME=$HOME/Documents/Work/clipos

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

clipos_cd() {
	cd $CLIPOS_HOME/$1
}


# colored man
man() {
	GROFF_NO_SGR=1 \
	LESS_TERMCAP_mb=$'\e[1;32m' \
	LESS_TERMCAP_md=$'\e[1;32m' \
	LESS_TERMCAP_me=$'\e[0m' \
	LESS_TERMCAP_se=$'\e[0m' \
	LESS_TERMCAP_so=$'\e[01;33m' \
	LESS_TERMCAP_ue=$'\e[0m' \
	LESS_TERMCAP_us=$'\e[1;4;31m' \
	command man "$@"
}

# copy/paste
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias open='xdg-open'
# ls aliases
alias ls='ls --color'
alias ll='ls -l'
alias la='ls -a'
alias l='ls'
alias ccd='clipos_cd'
