call plug#begin('~/.config/nvim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Install ctrlp plugin
Plug 'kien/ctrlp.vim'
" Install vim surround
Plug 'tpope/vim-surround'
Plug 'honza/vim-snippets'
" Gruvbox
Plug 'rafi/awesome-vim-colorschemes'
Plug 'kshenoy/vim-signature'
Plug 'easymotion/vim-easymotion'
call plug#end()
